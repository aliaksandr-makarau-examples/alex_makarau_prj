# alex_makarau_prj

Hi. My name is Aliaksandr ( Aleś / Alex ) Makarau ( Makaraŭ ) and this is my project on GitLab with some efforts to create something valuable with help of programming languages.  

Every sub-project is located in a different branch of the project:
 - [Black Jack](https://gitlab.com/aliaksandr-makarau-examples/alex_makarau_prj/-/tree/cpp_black_jack)
